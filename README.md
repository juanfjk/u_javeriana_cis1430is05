# README #

La aplicación de software que soporta el desarrollo del Trabajo de Grado que tiene por título “Apoyo a la Agricultura de Precisión en Colombia a partir de imágenes adquiridas desde Vehículos Aéreos no Tripulados (UAV’s)” ayuda al desarrollo del concepto de fotogrametría (medición sobre fotografías) para que dicha técnica pueda apoyar las decisiones tomadas desde la Agricultura de Precisión.

Dicha aplicación recibe como entrada una fotografía aérea tomada desde el UAV, esta fotografía tiene una imagen de la vista aérea de un área cubierta por una plantación agrícola, junto a un patrón de referencia. A través de la aplicación de software es posible dibujar dos polígonos, el primero corresponde al dibujo de un polígono rectangular que representa el área del patrón a partir de ello el Usuario debe ingresar el tamaño real del patrón y el segundo para inscribir en un polígono irregular el área del cultivo visualizado en la fotografía, la aplicación de software se encarga de calcular el área real del cultivo aplicando el método matemático modelado para el desarrollo del presente Trabajo de Grado que consiste en identificar el área de un polígono irregular. Finalmente el software brinda al Usuario el área y el perímetro del cultivo así como una estimación de la producción total de la cosecha.

Este repositorio se encuentra disponible con la finalidad de apoyar trabajos futuros en proyectos similares que se soporten sobre Agricultura de Precisión y UAV's.

### Acerca del repositorio ###

* Desarrollo de una aplicación de software que ayuda a soportar decisiones tomadas mediante la Agricultura de Precisión.
* Enlace del [sitio web](http://pegasus.javeriana.edu.co/~CIS1430IS05/)  
* V1.0

### Puesta a punto ###

* Lenguaje: Java
* IDE: NetBeans 7.4
* Respositorio: Git
* Gestor nube repositorio: Bitbucket
* Gestor local repositorio: SmartGit/Hg 6

###Contacto ###

* [Sitio web](http://pegasus.javeriana.edu.co/~CIS1430IS05/)  
* Pontificia Universidad Javeriana, Bogotá DC, Colombia

###Licencia ###
Los derechos patrimoniales estarán determinados por la siguiente licencia: Compartir para investigación y desarrollo del tema, estando regido por la licencias de Creative Commons

![CC-by-nc-sa.png](https://bitbucket.org/repo/KGqa57/images/3888196428-CC-by-nc-sa.png)

BY - Debe reconocer la autoría de la obra de la manera especificada por el autor o el licenciador.

NC - El licenciador permite copiar, distribuir y comunicar públicamente la obra. A cambio, esta obra no puede ser utilizada con finalidades comerciales -- a menos que se obtenga el permiso del licenciador.

SA - El licenciador le permite distribuir obras derivadas sólo bajo una licencia idéntica a la que regula su obra o una licencia compatible.