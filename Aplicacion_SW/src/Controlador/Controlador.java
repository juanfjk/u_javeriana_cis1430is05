/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controlador;

import Modelo.Cultivo;
import Modelo.Foto;
import Modelo.Patron;

/**
 *
 * @author juan
 */
public class Controlador {
 
    Cultivo cultivoControl = new Cultivo();
    Patron  patronControl  = new Patron();
    Foto    fotoControl    = new Foto();
    
    public void capturarPuntosCultivoControl (int x, int y)
    {
        cultivoControl.capturarPuntosCultivo(x, y);
    }
    
    public void capturarPuntosPatronCultivo (int x, int y)
    {
        patronControl.capturarPuntosPatron(x, y);
    }        
    
    public float calcularAreaCultivoFotoControl()
    {
        return cultivoControl.calcularAreaCultivoFoto();
    }
    
    public int[] generarPuntosPoligonoPatronXControl(){
        return patronControl.generarPuntosPoligonoPatronX();
    }
    
    public int[] generarPuntosPoligonoPatronYControl(){
        return patronControl.generarPuntosPoligonoPatronY();
    }
    
    public void borrarPuntosPatronControl(){
        patronControl.borrarPuntosPatron();
    }
    
    public void borrarPuntosCultivoControl(){
        cultivoControl.borrarPuntosCultivo();
    }
    
    public float calcularAreaPatronRealControl(float ancho, float alto){
        return patronControl.calcularAreaPatronReal(ancho, alto);
    }
    
    public float calcularAreaPatronFotoControl(){
        return patronControl.calcularAreaPatronFoto();
    }
    
    public int[] generarPuntosPoligonoCultivoXControl(){
        return cultivoControl.generarPuntosPoligonoCultivoX();
    }
    
    public int[] generarPuntosPoligonoCultivoYControl(){
        return cultivoControl.generarPuntosPoligonoCultivoY();
    }
    
    public int[] generarPuntosLineaCultivo (int contador){
        return cultivoControl.generarPuntosLineaCultivo(contador);
    }
    
    public int[] generarPuntosLineaCultivoUltimoPunto (int contador){
        return cultivoControl.generarPuntosLineaCultivoUltimoPunto(contador);
    }
    
    public int[] generarPuntosLineaPatron (int contador){
        return patronControl.generarPuntosLineaPatron(contador);
    }
    
    public int[] generarPuntosLineaPatronUltimoPunto (int contador){
        return patronControl.generarPuntosLineaPatronUltimoPunto(contador);
    }
    
    public void obtenerDatoProduccionCultivoControl(float produccion){
        cultivoControl.setProduccionDato(produccion);
    }
    
    public float getAreaPatronRealControl() {
        return patronControl.getAreaPatronReal();
    }
    
    public float getAreaPatronFotoControl() {
        return patronControl.getAreaPatronFoto();
    }
    
    public float calcularAreaCultivoRealControl(){
        return cultivoControl.calcularAreaCultivoReal(getAreaPatronFotoControl(), getAreaPatronRealControl());
    }
    
    public float calcularPerimetroCultivoRealControl (){
        return cultivoControl.calcularPerimetroCultivoReal();
    }
    
    public void  estadoDibujoPoligonoPatronControl( boolean estado){
        patronControl.setDibujoPoligoPatronActivo(estado);
    }
    
    public boolean obtenerEstadoDibujoPoligonoPatronControl (){
        return patronControl.isDibujoPoligoPatronActivo();
    }
    
    public void  estadoDibujoPoligonoCultivoControl( boolean estado){
        cultivoControl.setDibujoPoligoCultivoActivo(estado);
    }
    
    public boolean obtenerEstadoDibujoPoligonoCultivoControl (){
        return cultivoControl.isDibujoPoligoCultivoActivo();
    }
    
    public float calcularProduccionCultivoControl (){
        return cultivoControl.calcularProduccionCultivo();
    }
    
    public void cargarEstadoFotoCultivoControl(boolean estado){
        fotoControl.setFotoIngresadaSistema(estado);
    }
    
    public boolean obtenerEstadoFotoCultivoControl(){
        return fotoControl.isFotoIngresadaSistema();
    }
    
    public void borrarTodoPatronControl(){
        patronControl.borrarTodoPatron();
    }
    
    public void borrarTodoCultivoControl(){
        cultivoControl.borrarTodoCultivo();
    }
    
}
