/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

/**
 *
 * @author juan
 */
public class Foto {
    
    private boolean fotoIngresadaSistema;

    public Foto() {
    }

    public Foto(boolean fotoIngresadaSistema) {
        this.fotoIngresadaSistema = fotoIngresadaSistema;
    }


    public boolean isFotoIngresadaSistema() {
        return fotoIngresadaSistema;
    }

    public void setFotoIngresadaSistema(boolean fotoIngresadaSistema) {
        this.fotoIngresadaSistema = fotoIngresadaSistema;
    }

}
