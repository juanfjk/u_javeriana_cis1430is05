/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

import java.util.ArrayList;

/**
 *
 * @author juan
 */
public class Cultivo {
    
    private ArrayList<Punto> puntosCultivo = new ArrayList<Punto>();
    private float areaCultivoReal;
    private float areaCultivoFoto;
    private float perimetroCultivoReal;
    private float perimetroCultivoFoto;
    private float produccionDato;
    private float produccionCultivoReal;
    private boolean dibujoPoligoCultivoActivo;


    public Cultivo() {
    }
    
    public Cultivo(ArrayList<Punto> puntosCultivo, float areaCultivoReal, float areaCultivoFoto, float perimetroCultivoReal, float perimetroCultivoFoto, float produccionDato, boolean dibujoPoligoCultivoActivo, float produccionCultivoReal) {
        this.puntosCultivo = new ArrayList<>();
        this.areaCultivoReal = areaCultivoReal;
        this.areaCultivoFoto = areaCultivoFoto;
        this.perimetroCultivoReal = perimetroCultivoReal;
        this.perimetroCultivoFoto = perimetroCultivoFoto;
        this.produccionDato = produccionDato;
        this.dibujoPoligoCultivoActivo = dibujoPoligoCultivoActivo;
        this.produccionCultivoReal = produccionCultivoReal;
    }

    public float getProduccionCultivoReal() {
        return produccionCultivoReal;
    }

    public void setProduccionCultivoReal(float produccionCultivoReal) {
        this.produccionCultivoReal = produccionCultivoReal;
    }
    
    public boolean isDibujoPoligoCultivoActivo() {
        return dibujoPoligoCultivoActivo;
    }

    public void setDibujoPoligoCultivoActivo(boolean dibujoPoligoCultivoActivo) {
        this.dibujoPoligoCultivoActivo = dibujoPoligoCultivoActivo;
    }
    
    public float getProduccionDato() {
        return produccionDato;
    }

    public void setProduccionDato(float produccionDato) {
        this.produccionDato = produccionDato;
    }

    public ArrayList<Punto> getPuntosCultivo() {
        return puntosCultivo;
    }

    public void setPuntosCultivo(ArrayList<Punto> puntosCultivo) {
        this.puntosCultivo = puntosCultivo;
    }

    public float getAreaCultivoReal() {
        return areaCultivoReal;
    }

    public void setAreaCultivoReal(float areaCultivoReal) {
        this.areaCultivoReal = areaCultivoReal;
    }

    public float getAreaCultivoFoto() {
        return areaCultivoFoto;
    }

    public void setAreaCultivoFoto(float areaCultivoFoto) {
        this.areaCultivoFoto = areaCultivoFoto;
    }

    public float getPerimetroCultivoReal() {
        return perimetroCultivoReal;
    }

    public void setPerimetroCultivoReal(float perimetroCultivoReal) {
        this.perimetroCultivoReal = perimetroCultivoReal;
    }

    public float getPerimetroCultivoFoto() {
        return perimetroCultivoFoto;
    }

    public void setPerimetroCultivoFoto(float perimetroCultivoFoto) {
        this.perimetroCultivoFoto = perimetroCultivoFoto;
    }
    
    public void capturarPuntosCultivo (int x, int y)
    {
        Punto p = new Punto(x,y);
        puntosCultivo.add(p);
        System.out.println("Punto cultivo recibido de la vista: " + p.getX() + ", " + p.getY());
    }
    
    public float calcularAreaCultivoFoto ()
    {
        
        float sumatoria = 0;
        
        if (puntosCultivo.size()>=3){
        
            for(int i=0; i<puntosCultivo.size()-1; i++){
                sumatoria += (puntosCultivo.get(i).getX() * puntosCultivo.get(i+1).getY()) - (puntosCultivo.get(i).getY() * puntosCultivo.get(i+1).getX());
            }
            
            int ultimo = puntosCultivo.size()-1;
            
            sumatoria += (puntosCultivo.get(ultimo).getX() * puntosCultivo.get(0).getY()) - (puntosCultivo.get(ultimo).getY() * puntosCultivo.get(0).getX());
        }
        
        setAreaCultivoFoto(Math.abs(sumatoria)/2);
        
        System.out.println("Area cultivo foto: " + getAreaCultivoFoto());
        
        return getAreaCultivoFoto();
    }
    
    public float calcularAreaCultivoReal (float areaPatronFoto, float areaPatronReal){
        
        float areaReal = (getAreaCultivoFoto()*areaPatronReal)/areaPatronFoto;
        
        setAreaCultivoReal(areaReal);
        
        return getAreaCultivoReal();
    }
            
    
    public int[] generarPuntosPoligonoCultivoX()
    {
        int [] x = new int[puntosCultivo.size()];

        for (int i=0; i<puntosCultivo.size(); i++)
        {
            x[i] = puntosCultivo.get(i).getX();
        }
        
//        for (int i=0; i<x.length; i++)
//        {
//            System.out.println(x[i]);
//        }
        
        return x;
    }
    
    public int[] generarPuntosPoligonoCultivoY()
    {
        int [] y = new int[puntosCultivo.size()];

        for (int i=0; i<puntosCultivo.size(); i++)
        {
            y[i] = puntosCultivo.get(i).getY();
        }
        
//        for (int i=0; i<y.length; i++)
//        {
//            System.out.println(y[i]);
//        }
        
        return y;
    }
    
    public int[] generarPuntosLineaCultivo (int contador){
    
        int [] linea = new int[4];
        
        //Obtener el anterior
        contador--;
        
        linea[0] = puntosCultivo.get(contador-1).getX();
        linea[1] = puntosCultivo.get(contador-1).getY();
        linea[2] = puntosCultivo.get(contador).getX();
        linea[3] = puntosCultivo.get(contador).getY();
        
        return linea;
    }
    
    public int[] generarPuntosLineaCultivoUltimoPunto (int contador){
    
        int [] linea = new int[4];
        
        linea[0] = puntosCultivo.get(contador-1).getX();
        linea[1] = puntosCultivo.get(contador-1).getY();
        linea[2] = puntosCultivo.get(0).getX();
        linea[3] = puntosCultivo.get(0).getY();
        
        return linea;
    }
    
    public void obtenerDatoProduccionCultivo(float produccion){
        setProduccionDato(produccion);
    }
    
    public float calcularPerimetroCultivoFoto (){
        
        float perimetroFoto = 0;
        
        for(int i=0; i<puntosCultivo.size()-1; i++){
            float x = (float) Math.pow((puntosCultivo.get(i).getX() - puntosCultivo.get(i+1).getX()),2);
            float y = (float) Math.pow((puntosCultivo.get(i).getY() - puntosCultivo.get(i+1).getY()),2);
            perimetroFoto += (float) Math.pow((x+y),0.5);
        }
        
        int ultimo = puntosCultivo.size()-1;
        float x = (float) Math.pow((puntosCultivo.get(0).getX() - puntosCultivo.get(ultimo).getX()),2);
        float y = (float) Math.pow((puntosCultivo.get(0).getY() - puntosCultivo.get(ultimo).getY()),2);
        perimetroFoto += (float) Math.pow((x+y),0.5);
        
        setPerimetroCultivoFoto(perimetroFoto);
        
        return getPerimetroCultivoFoto();
    }
    
    public float calcularPerimetroCultivoReal (){
        
        calcularPerimetroCultivoFoto();
        
        float relacion = ((float)Math.pow(getAreaCultivoFoto(),0.5)) / ((float)Math.pow(getAreaCultivoReal(),0.5));
        float perimetroReal = getPerimetroCultivoFoto() / relacion;
        
        setPerimetroCultivoReal(perimetroReal);
        
        return getPerimetroCultivoReal();
    }
    
    public void borrarPuntosCultivo()
    {
        puntosCultivo.clear();
    }
    
    public float calcularProduccionCultivo ()
    {
        float valor = getAreaCultivoReal()*getProduccionDato();
        setProduccionCultivoReal(valor);
    
        return getProduccionCultivoReal();
    }
    
    public void borrarTodoCultivo()
    {
        puntosCultivo.clear();
        setAreaCultivoFoto(0);
        setAreaCultivoReal(0);
        setDibujoPoligoCultivoActivo(false);
        setPerimetroCultivoFoto(0);
        setPerimetroCultivoReal(0);
        setProduccionCultivoReal(0);
        setProduccionDato(0);
    }
}
