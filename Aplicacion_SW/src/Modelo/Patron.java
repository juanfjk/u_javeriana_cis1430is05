/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modelo;

import java.util.ArrayList;

/**
 *
 * @author juan
 */
public class Patron {
    
    private ArrayList<Punto> puntosPatron = new ArrayList<>();
    private float anchoPatron;
    private float altoPatron;
    private float areaPatronReal;
    private float areaPatronFoto;
    private boolean dibujoPoligoPatronActivo;

    public Patron() {
    }
    
    public Patron(ArrayList<Punto> puntosPatron, float anchoPatron, float altoPatron, float areaPatronReal, float areaPatronFoto, boolean dibujoPoligoPatronActivo) {
        this.puntosPatron = puntosPatron;
        this.anchoPatron = anchoPatron;
        this.altoPatron = altoPatron;
        this.areaPatronReal = areaPatronReal;
        this.areaPatronFoto = areaPatronFoto;
        this.dibujoPoligoPatronActivo = dibujoPoligoPatronActivo;
    }

    public boolean isDibujoPoligoPatronActivo() {
        return dibujoPoligoPatronActivo;
    }

    public void setDibujoPoligoPatronActivo(boolean dibujoPoligoPatronActivo) {
        this.dibujoPoligoPatronActivo = dibujoPoligoPatronActivo;
    }
    
    public ArrayList<Punto> getPuntosPatron() {
        return puntosPatron;
    }

    public void setPuntosPatron(ArrayList<Punto> puntosPatron) {
        this.puntosPatron = puntosPatron;
    }

    public float getAnchoPatron() {
        return anchoPatron;
    }

    public void setAnchoPatron(float anchoPatron) {
        this.anchoPatron = anchoPatron;
    }

    public float getAltoPatron() {
        return altoPatron;
    }

    public void setAltoPatron(float altoPatron) {
        this.altoPatron = altoPatron;
    }

    public float getAreaPatronReal() {
        return areaPatronReal;
    }

    public void setAreaPatronReal(float areaPatronReal) {
        this.areaPatronReal = areaPatronReal;
    }

    public float getAreaPatronFoto() {
        return areaPatronFoto;
    }

    public void setAreaPatronFoto(float areaPatronFoto) {
        this.areaPatronFoto = areaPatronFoto;
    }
    
    public void capturarPuntosPatron (int x, int y)
    {
        Punto p = new Punto(x,y);
        puntosPatron.add(p);
        System.out.println("Punto patron recibido de la vista: " + p.getX() + ", " + p.getY());
        
        if(puntosPatron.size()==4)
        {   
            generarPuntosPoligonoPatronX();
            generarPuntosPoligonoPatronY();
            calcularAreaPatronFoto();
        }
    }
    
    public int[] generarPuntosPoligonoPatronX()
    {
        int [] x = new int[4];
        
        x[0] = puntosPatron.get(0).getX();
        x[1] = puntosPatron.get(1).getX();
        x[2] = puntosPatron.get(2).getX();
        x[3] = puntosPatron.get(3).getX();
        
//        for (int i=0; i<4; i++)
//        {
//            System.out.println(x[i]);
//        }
        
        return x;
    }
    
    public int[] generarPuntosPoligonoPatronY()
    {
        int [] y = new int[4];
        
        y[0] = puntosPatron.get(0).getY();
        y[1] = puntosPatron.get(1).getY();
        y[2] = puntosPatron.get(2).getY();
        y[3] = puntosPatron.get(3).getY();
        
//        for (int i=0; i<4; i++)
//        {
//            System.out.println(y[i]);
//        }
        
        return y;
    }
    
    
    public void borrarPuntosPatron()
    {
        puntosPatron.clear();
    }
    
    public int[] generarPuntosLineaPatron (int contador){
    
        int [] linea = new int[4];
        
        //Obtener el anterior
        contador--;
        
        linea[0] = puntosPatron.get(contador-1).getX();
        linea[1] = puntosPatron.get(contador-1).getY();
        linea[2] = puntosPatron.get(contador).getX();
        linea[3] = puntosPatron.get(contador).getY();
        
        return linea;
    }
    
    public int[] generarPuntosLineaPatronUltimoPunto (int contador){
    
        int [] linea = new int[4];
        
        linea[0] = puntosPatron.get(contador-1).getX();
        linea[1] = puntosPatron.get(contador-1).getY();
        linea[2] = puntosPatron.get(0).getX();
        linea[3] = puntosPatron.get(0).getY();
        
        return linea;
    }
    
    public float calcularAreaPatronReal(float ancho, float alto)
    {
        float area = ancho * alto;
        setAreaPatronReal(area);
        
        System.out.println("Area patron real: "+getAreaPatronReal());
        
        return getAreaPatronReal();
    }
    
    public float calcularAreaPatronFoto()
    {
        float sumatoria = 0;
        
        if (puntosPatron.size()==4){
        
            for(int i=0; i<puntosPatron.size()-1; i++){
                sumatoria += (puntosPatron.get(i).getX() * puntosPatron.get(i+1).getY()) - (puntosPatron.get(i).getY() * puntosPatron.get(i+1).getX());
            }
            
            int ultimo = puntosPatron.size()-1;
            
            sumatoria += (puntosPatron.get(ultimo).getX() * puntosPatron.get(0).getY()) - (puntosPatron.get(ultimo).getY() * puntosPatron.get(0).getX());
        }
        
        setAreaPatronFoto(Math.abs(sumatoria)/2);
        
        System.out.println("Area patron foto: " + getAreaPatronFoto());
        
        return getAreaPatronFoto();
    }
    
    public void borrarTodoPatron()
    {
        puntosPatron.clear();
        setAltoPatron(0);
        setAnchoPatron(0);
        setAreaPatronFoto(0);
        setAreaPatronReal(0);
        setDibujoPoligoPatronActivo(false);
    }
}

