/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Vista;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author juan
 */
public class Dibujo {
    
    public static void hacerPoligoPatron (Graphics g, int [] x, int [] y , int puntos)
    {
        Color c=new Color(0f,0f,1f,.4f );
        g.setColor(c);
        g.fillPolygon(x, y, puntos);
    }
    
    public static void hacerPuntoPatron (Graphics g, int x, int y, int dia)
    {
        g.setColor(Color.BLUE);
        g.fillOval(x, y, dia, dia);
    }
    
    public static void hacerPoligoCultivo (Graphics g, int [] x, int [] y , int puntos)
    {
        Color c=new Color(1f,0f,0f,.4f );
        g.setColor(c);
        g.fillPolygon(x, y, puntos);
    }
    
    public static void hacerPuntoCultivo (Graphics g, int x, int y, int dia)
    {
        g.setColor(Color.RED);
        g.fillOval(x, y, dia, dia);
    }
    
    public static void hacerLineaCultivo(Graphics g, int x, int y, int x1, int y1)
    {
        g.setColor(Color.RED);
        g.drawLine(x, y, x1, y1);
    }
    
    public static void hacerLineaPatron(Graphics g, int x, int y, int x1, int y1)
    {
        g.setColor(Color.BLUE);
        g.drawLine(x, y, x1, y1);
    }
}
